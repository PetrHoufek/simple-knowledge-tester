package start;
import gui.GUI;
import logic.FileCreator;
import logic.ResultHistory;
import logic.SuccessCalculator;
import logic.TextFileIO;

/**
 * This class holds references to objects
 * 
 * @author Petr Honke-Houfek
 *
 */
public class Start {
	private GUI gui;
	private FileCreator fileCreator;
	private TextFileIO textFileIO;
	private SuccessCalculator successCalculator;
	private ResultHistory resultHistory;
	private final String[] forbiddenDirectories = {"C:","C:\\","C:\\Program Files","C:\\Program Files\\","C:\\Program Files (x86)","C:\\Program Files (x86)\\","C:\\Program Files (x64)",
			"C:\\Program Files (x64)\\","C:\\Users","C:\\Users\\","C:\\Windows","C:\\Windows\\"};
	
	public Start(){
		gui = new GUI(this);
	}

	public GUI getGui() {
		return gui;
	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}

	public void restartApplication(){
		textFileIO.restartTFIO();
		gui.showInitialWindow();
	}

	public TextFileIO getTextFileIO() {
		return textFileIO;
	}

	public void setTextFileIO(TextFileIO textFileIO) {
		this.textFileIO = textFileIO;
	}
	
	public FileCreator getFileCreator() {
		return fileCreator;
	}

	public void setFileCreator(FileCreator fileCreator) {
		this.fileCreator = fileCreator;
	}
	
	public ResultHistory getResultHistory() {
		return resultHistory;
	}

	public void setResultHistory(ResultHistory resultHistory) {
		this.resultHistory = resultHistory;
	}
	
	public SuccessCalculator getSuccessCalculator() {
		return successCalculator;
	}
	
	public void setSuccessCalculator(SuccessCalculator successCalculator) {
		this.successCalculator = successCalculator;
	}

	public String[] getForbiddenDirectories() {
		return forbiddenDirectories;
	}
	
	
	
	public static void main(String[] args){
		new Start();
	}
}
