package logic;

import gui.GUI;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;

import start.Start;

/**
 * This class is for creating expressions sets using this application
 * 
 * @author Petr Honke-Houfek
 * 
 */
public class FileCreator {

	private Start start;
	private GUI gui;
	private StringBuilder first = new StringBuilder();
	private StringBuilder second = new StringBuilder();
	private StringBuilder message = new StringBuilder();
	private Color color;
	private HashSet<String> set = new HashSet<String>();
	private HashMap<String, String> map = new HashMap<String, String>();
	private boolean announcement;

	/**
	 * Constructor
	 * 
	 * @param start
	 */
	public FileCreator(Start start) {

		this.start = start;
		gui = start.getGui();
	}

	/**
	 * Adds another pair of words to your new file
	 */
	public void addWord() {

		first.setLength(0);
		second.setLength(0);
		message.setLength(0);

		first.append(gui.getNewFileTF1().getText().trim());
		second.append(gui.getNewFileTF2().getText().trim());

		if (first.toString().trim().isEmpty() && second.toString().trim().isEmpty()) {
			message.append(" Both words are missing!");
			color = Color.RED;
		} else {
			if (first.toString().trim().isEmpty()) {
				message.append(" First word is missing!");
				color = Color.RED;
			}
			if (second.toString().trim().isEmpty()) {
				message.append(" Second word is missing!");
				color = Color.RED;
			}
		}

		if (!first.toString().trim().isEmpty() && !second.toString().trim().isEmpty()) {
			
			color = Color.DARK_GRAY;
			set.add(first.toString());
			map.put(first.toString(), second.toString());
			
			if (first.length() <= 7) {
				message.append(" " + first + "		-		" + second);
			} else {
				message.append(" " + first + "	-		" + second);
			}
			gui.getNewFileTF1().setText("");
			gui.getNewFileTF2().setText("");
		}

		if (announcement) {
			gui.getMyPane().setText("");
			announcement = false;
		}

		gui.getMyPane().newLine();
		gui.getMyPane().addString(message.toString(), color, false, 12);

	}
	
	/**
	 * Creates the file, clears JtextArea, HashSet and HashMap and loads the file
	 */
	public void createFile() {

		if (!map.isEmpty()) {
			if (!gui.getNewFileTF3().getText().trim().isEmpty()) {
				start.getTextFileIO().exportFile(gui.getNewFileTF3().getText());
				start.getTextFileIO().exportExpressions(set, map);
				start.getTextFileIO().checksFilePath(start.getTextFileIO().getOutputFile().getAbsolutePath());
				set.clear();
				map.clear();
				gui.getMyPane().setText("");
				gui.getMyPane().addString("File created !", new Color(27, 180, 31), true);
				announcement = true;
			}
		}
	}

	public HashSet<String> getSet() {

		return set;
	}

	public HashMap<String, String> getMap() {

		return map;
	}

}
