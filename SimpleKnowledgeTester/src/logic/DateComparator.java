package logic;

import java.util.Comparator;

/**
 * Comparing dates
 * 
 * @author Petr Honke-Houfek
 *
 */
public class DateComparator implements Comparator<String>{
	
	private StringBuilder yearO1 = new StringBuilder();
	private StringBuilder yearO2 = new StringBuilder();
	private StringBuilder monthO1 = new StringBuilder();
	private StringBuilder monthO2 = new StringBuilder();
	private StringBuilder dayO1 = new StringBuilder();
	private StringBuilder dayO2 = new StringBuilder();
	private StringBuilder hourO1 = new StringBuilder();
	private StringBuilder hourO2 = new StringBuilder();
	private StringBuilder minuteO1 = new StringBuilder();
	private StringBuilder minuteO2 = new StringBuilder();
	
	@Override
	public int compare(String o1, String o2){
		yearO1.setLength(0);
		yearO2.setLength(0);
		monthO1.setLength(0);
		monthO2.setLength(0);
		dayO1.setLength(0);
		dayO2.setLength(0);
		hourO1.setLength(0);
		hourO2.setLength(0);
		minuteO1.setLength(0);
		minuteO2.setLength(0);
		
		minuteO1.append(o1.substring( (o1.indexOf(':')) + 1, o1.length()));
		minuteO2.append(o2.substring( (o2.indexOf(':')) + 1, o2.length()));
		hourO1.append(o1.substring((o1.indexOf(' ')) + 1, o1.indexOf(':')));
		hourO2.append(o2.substring((o2.indexOf(' ')) + 1, o2.indexOf(':')));
		yearO1.append(o1.substring(( (o1.lastIndexOf('.')) + 1), o1.indexOf(' ')));
		yearO2.append(o2.substring(( (o2.lastIndexOf('.')) + 1), o2.indexOf(' ')));
		monthO1.append(o1.substring(( (o1.indexOf('.')) + 1), o1.lastIndexOf('.')));
		monthO2.append(o2.substring(( (o2.indexOf('.')) + 1), o2.lastIndexOf('.')));
		dayO1.append(o1.substring(0, o1.indexOf('.')));
		dayO2.append(o2.substring(0, o2.indexOf('.')));
		
		//year
		if(Integer.parseInt(yearO1.toString()) > Integer.parseInt(yearO2.toString())){
			return 1;
		}else if(Integer.parseInt(yearO1.toString()) < Integer.parseInt(yearO2.toString())){
			return -1;
		}else{
			
			//month
			if(Integer.parseInt(monthO1.toString()) > Integer.parseInt(monthO2.toString())){
				return 1;
			}else if(Integer.parseInt(monthO1.toString()) < Integer.parseInt(monthO2.toString())){
				return -1;
			}else{
				
				//day
				if(Integer.parseInt(dayO1.toString()) > Integer.parseInt(dayO2.toString())){
					return 1;
				}else if(Integer.parseInt(dayO1.toString()) < Integer.parseInt(dayO2.toString())){
					return -1;
				}else{
					
					//hour
					if(Integer.parseInt(hourO1.toString()) > Integer.parseInt(hourO2.toString())){
						return 1;
					}else if(Integer.parseInt(hourO1.toString()) > Integer.parseInt(hourO2.toString())){
						return -1;
					}else{
						
						//minute
						if(Integer.parseInt(minuteO1.toString()) > Integer.parseInt(minuteO2.toString())){
							return 1;
						}else if(Integer.parseInt(minuteO1.toString()) > Integer.parseInt(minuteO2.toString())){
							return -1;
						}else{
							return 0;
						}
					}
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
