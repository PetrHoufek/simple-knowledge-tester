package logic;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import start.Start;

/**
 * Assignment 1 ResultHistory.java Purpose: Recording test success
 * 
 * @author Petr Honke-Houfek
 * @version 1.0 7/5/2014
 */
public class ResultHistory {
	
	private Start start;
	private File homeDirectory;
	private File resultsFile;
	private ArrayList<Object[]> data;
	
	/**
	 * Constructor
	 * @param start
	 */
	public ResultHistory(Start start){
		this.start = start;
		start.setResultHistory(this);
		homeDirectory = new File(System.getProperty("user.home") + File.separator + "Simple Knowledge Tester");
		resultsFile = new File(homeDirectory + File.separator + "results.dat");
		importFile();
	}
	
	/**
	 * Imports data from results.dat and loads them into "data" ArrayList
	 */
	public void importFile(){
		if(!homeDirectory.exists()){
			homeDirectory.mkdir();
		}
		data = new ArrayList<Object[]>();
		
		if(resultsFile.exists()){
			if(resultsFile.length() > 0){
				try {
					DataInputStream input = new DataInputStream(new BufferedInputStream(new FileInputStream(resultsFile)));
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm");
					StringBuilder name = new StringBuilder();
					StringBuilder success = new StringBuilder();
					StringBuilder date = new StringBuilder();
					StringBuilder day = new StringBuilder();
					StringBuilder month = new StringBuilder();
					StringBuilder year = new StringBuilder();
					StringBuilder hour = new StringBuilder();
					StringBuilder minute = new StringBuilder();
					
					while(input.available() >0){
						name.append(input.readUTF());
						success.append(input.readInt());
						date.append(input.readUTF());
						
						day.append(date.substring(0, date.indexOf(".")));
						month.append(date.substring( (date.indexOf(".") + 1), date.lastIndexOf(".")));
						year.append(date.substring( (date.lastIndexOf(".") + 1), date.indexOf(" ")));
						hour.append(date.substring( (date.indexOf(" ")) + 1, date.indexOf(":")));
						minute.append(date.substring( (date.indexOf(":")) + 1, date.length()));
						
						cal.set(Integer.parseInt(year.toString()), Integer.parseInt(month.toString()) - 1, Integer.parseInt(day.toString()), Integer.parseInt(hour.toString()), Integer.parseInt(minute.toString()));
						data.add(new Object[]{name.toString(), Integer.parseInt(success.toString()), sdf.format(cal.getTime())});				
						
						name.setLength(0);
						success.setLength(0);
						date.setLength(0);
						day.setLength(0);
						month.setLength(0);
						year.setLength(0);
						hour.setLength(0);
						minute.setLength(0);
					}
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}else{
			try {
				resultsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Add new record to records.dat
	 */
	public void addRecord(){
		try {
			String name;
			if(start.getTextFileIO().isOutputCreated()){
				name = start.getTextFileIO().getOutputFile().getName().substring(0, start.getTextFileIO().getOutputFile().getName().lastIndexOf('.'));	//if there were wrong answers
			}else{
				name = start.getTextFileIO().getSourceFile().getName().substring(0, start.getTextFileIO().getSourceFile().getName().lastIndexOf('.'));	//if test was 100% successful
			}
			DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(resultsFile, true)));
			Calendar cal = Calendar.getInstance();
			Integer success = start.getSuccessCalculator().getResult();
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm");
			data.add(new Object[]{name.toString(), success, sdf.format(cal.getTime())});
			
			output.writeUTF(name);
			output.writeInt(success);
			output.writeUTF(sdf.format(cal.getTime()));
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void deleteHistory(){
		data.clear();
		if(resultsFile.exists()){
			try {
				PrintWriter writer = new PrintWriter(resultsFile);
				writer.write("");
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public ArrayList<Object[]> getData() {
		return data;
	}
}
