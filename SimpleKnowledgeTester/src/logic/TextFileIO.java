package logic;

import gui.GUI;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import start.Start;

/**
 * Assignment 1 ApplicationPlan.java
 * Purpose: This class handles creating / loading / reading all text files
 * 
 * @author Petr Honke-Houfek
 * 
 */
public class TextFileIO {
	
	private Start start;
	private GUI gui;
	private ArrayList<String> inputSet1;
	private ArrayList<String> inputSet2;
	private HashSet<String> outputSet;
	private HashMap<String, String> regularMap;
	private HashMap<String, String> reverseMap;
	private Iterator<String> iterator1;
	private Iterator<String> iterator2;
	private final File defFolder = new File(System.getProperty("user.home") + File.separator + "Simple Knowledge Tester");
	private File sourceFile;
	private File outputFile;
	private File settings;
	private final String defFolderPath = System.getProperty("user.home") + File.separator + "Simple Knowledge Tester";
	private String oldOutputPath;
	private String newOutputPath;
	private String sourcePath;
	private StringBuilder currentExpression;
	private boolean outputCreated;
	private boolean usingDefFolder = false;
	private boolean usingInFolder = false;
	private boolean usingRandFolder = false;
	private boolean settingsChanged = false;
	private boolean reverseTranslations = false;
	private boolean hasIteratorNext;
	private PrintWriter output;
	private RandomAccessFile raf;

	public TextFileIO(Start start){
		this.start = start;
		start.setTextFileIO(this);
		
		gui = this.start.getGui();
		init();
	}
	
	
	public void init(){
		outputCreated = false;
		hasIteratorNext = true;
		currentExpression = new StringBuilder();
		inputSet1 = new ArrayList<String>();
		inputSet2 = new ArrayList<String>();
		outputSet = new HashSet<String>();
		regularMap = new HashMap<String, String>();
		reverseMap = new HashMap<String, String>();
		settings = new File(defFolder.getPath() + File.separator + "settings.dat");
		importSettings();
	}
	
	/**
	 * Sets the application for another test
	 */
	public void restartTFIO(){
		outputCreated = false;
		hasIteratorNext = true;
		currentExpression = new StringBuilder();
		outputSet = new HashSet<String>();
		iterator1 = inputSet1.iterator();
		iterator2 = inputSet2.iterator();
	}

	/**
	 * Checks if the file exist and calls the method for data import
	 * 
	 * @param sourcePath
	 */
	public void checksFilePath(String sourcePath) {
		init();
		if (!sourcePath.trim().equals("")) {
			sourceFile = new File(sourcePath);
			if (sourceFile.exists()) {
				try {
					
					importTranslations();
					gui.isFileLoaded().setForeground(new Color(10, 195, 0));
					gui.isFileLoaded().setText("File loaded !");
					gui.getRegularOrder().setEnabled(true);
					gui.getReverseOrder().setEnabled(true);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else {
				gui.isFileLoaded().setForeground(Color.RED);
				gui.isFileLoaded().setText("ERROR: File not found !");
				gui.isFileLoaded().setVisible(true);
			}

		} else {
			gui.isFileLoaded().setForeground(Color.RED);
			gui.isFileLoaded().setText("You need to enter a path to the file !");
			gui.isFileLoaded().setVisible(true);
		}
	}

	/**
	 * Imports values from text file into a set and a map
	 * 
	 * @throws FileNotFoundException
	 */
	public void importTranslations() throws FileNotFoundException {
		StringBuilder key = new StringBuilder();
		StringBuilder value = new StringBuilder();
		Scanner input = new Scanner(sourceFile);

		while (input.hasNext()) {
			
			input.useDelimiter("-");
			key.setLength(0);
			key.append(input.next().trim());
			inputSet1.add(Character.toUpperCase(key.toString().charAt(0)) + key.toString().substring(1));
			
			if (input.hasNext()) {
				value.setLength(0);
				value.append(input.nextLine().replaceAll("[-]", "").trim());
				inputSet2.add(Character.toUpperCase(value.toString().charAt(0)) + value.toString().substring(1).toLowerCase());
				if (!value.toString().equals("")) { //if second string isn't empty
					regularMap.put(Character.toUpperCase(key.toString().charAt(0)) + key.toString().substring(1).toLowerCase(), Character.toUpperCase(value.toString().charAt(0)) + value.toString().substring(1).toLowerCase());
					reverseMap.put(Character.toUpperCase(value.toString().charAt(0)) + value.toString().substring(1).toLowerCase(), Character.toUpperCase(key.toString().charAt(0)) + key.toString().substring(1).toLowerCase());
				} else {
					regularMap.put(Character.toUpperCase(key.toString().charAt(0)) + key.toString().substring(1).toLowerCase(), "Undefined");
					reverseMap.put(Character.toUpperCase(value.toString().charAt(0)) + value.toString().substring(1).toLowerCase(), "Undefined");
				}
			} else {
				regularMap.put(Character.toUpperCase(key.toString().charAt(0)) + key.toString().substring(1).toLowerCase(), "Undefined");
				reverseMap.put("Undefined", Character.toUpperCase(key.toString().charAt(0)) + key.toString().substring(1).toLowerCase());
			}
		}
		
		input.close();
		Collections.shuffle(inputSet1);
		Collections.shuffle(inputSet2);
		iterator1 = inputSet1.iterator();
		iterator2 = inputSet2.iterator();
	}

	/**
	 * Checks if the outputFile exists, if so, creates newer version and sets the output for PrintWriter.
	 */
	public void exportFile(String name) {
		int number = 1;
		
		if(usingDefFolder == true){
			newOutputPath = defFolderPath;
			outputFile = new File(newOutputPath + File.separator + name + ".txt");
		}
		if(usingInFolder == true){
			if (sourceFile.getParentFile().isDirectory()) {
				outputFile = new File(sourceFile.getParentFile() + File.separator + name + ".txt");
				newOutputPath = sourceFile.getParentFile() + File.separator;
			}else{
				System.out.println("ERROR > SourceFile doesn't have valid directory");
			}
		}
		if(usingRandFolder == true){
			if(settingsChanged == false){
				newOutputPath = oldOutputPath;
			}
			outputFile = new File(newOutputPath + File.separator + name + ".txt");
		}
		while (outputFile.exists()) {
			number++;
			outputFile = new File(newOutputPath + File.separator + name + " (" + number + ").txt");
		}
		
		try {
			output = new PrintWriter(outputFile);
			outputCreated = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Imports settings if exists, otherwise uses default values.
	 */
	public void importSettings(){
		if(!defFolder.exists()){
			defFolder.mkdir();
		}
		
		try{
			if(settings.exists()){
				if(settings.length() > 0){
					raf = new RandomAccessFile(settings, "rw");
					usingDefFolder = raf.readBoolean();
					usingInFolder = raf.readBoolean();
					usingRandFolder = raf.readBoolean();
					oldOutputPath = raf.readUTF();
				}else{
					raf = new RandomAccessFile(settings, "rw");
					oldOutputPath = defFolderPath;
					usingDefFolder = true;
				}
			}else{
				raf = new RandomAccessFile(settings, "rw");
				oldOutputPath = defFolderPath;
				usingDefFolder = true;
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves the settings in selected folder
	 */
	public void exportSettings(){
		try {
			raf.setLength(0);
			raf.writeBoolean(usingDefFolder);
			raf.writeBoolean(usingInFolder);
			raf.writeBoolean(usingRandFolder);
			if(newOutputPath != null){
				raf.writeUTF(newOutputPath);
			}else{
				raf.writeUTF(defFolderPath);
			}
			raf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Changes the expressionLabel text to another expression(word)
	 */
	public void nextExpression() {
		if(reverseTranslations == false){
			if (iterator1.hasNext()) {
				currentExpression.setLength(0);
				currentExpression.append(iterator1.next());
				gui.getWordLabel1().setText(currentExpression.toString().toUpperCase().charAt(0) + currentExpression.toString().substring(1).toLowerCase());
				gui.getLabelPanel().revalidate();
			}else{
				hasIteratorNext = false;
			}
		}else{
			if (iterator2.hasNext()) {
				currentExpression.setLength(0);
				currentExpression.append(iterator2.next());
				gui.getWordLabel1().setText(currentExpression.toString().toUpperCase().charAt(0) + currentExpression.toString().substring(1).toLowerCase());
				gui.getLabelPanel().revalidate();
			}else{
				hasIteratorNext = false;
			}
		}
	}

	/**
	 * Writes pairs of expressions into a new file
	 */
	public void exportExpressions(HashSet<String> set, HashMap<String, String> map) {
		if (outputCreated == true) {
			for (String o : set) {
				if(o.length() <= 7){
					output.println(o + "			-			" + map.get(o));
				}else if(o.length() <= 15){
					output.println(o + "		-			" + map.get(o));
				}else{
					output.println(o + "	-			" + map.get(o));
				}
			}
			output.close();
		}
	}

	public HashSet<String> getOutputSet() {
		return outputSet;
	}

	public StringBuilder getCurrentExpression() {
		return currentExpression;
	}

	public PrintWriter getOutput() {
		return output;
	}

	public boolean isOutputCreated() {
		return outputCreated;
	}

	/**
	 * Change outputCreated, prevents NullPointerException in method exportExpressions() if program is closed before initialising PrintWriter
	 * 
	 * @param outputCreated
	 */
	public void setOutputCreated(boolean outputCreated) {
		this.outputCreated = outputCreated;
	}

	public HashMap<String, String> getMap() {
		if(reverseTranslations == false){
			return regularMap;
		}else{
			return reverseMap;
		}
	}

	public Iterator<String> getIterator() {
		if(reverseTranslations == false){
			return iterator1;
		}else{
			return iterator2;
		}
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public boolean isUsingDefFolder() {
		return usingDefFolder;
	}

	public void setUsingDefFolder() {
		usingInFolder = false;
		usingDefFolder = true;
		usingRandFolder = false;
	}

	public boolean isUsingInFolder() {
		return usingInFolder;
	}

	public void setUsingInFolder() {
		usingInFolder = true;
		usingDefFolder = false;
		usingRandFolder = false;
	}

	public boolean isUsingRandFolder() {
		return usingRandFolder;
	}

	public void setUsingRandFolder() {
		usingInFolder = false;
		usingDefFolder = false;
		usingRandFolder = true;
	}
	
	public String getNewOutputPath() {
		return newOutputPath;
	}

	public void setNewOutputPath(String newOutputPath) {
		this.newOutputPath = newOutputPath;
	}

	public void setSettingsChanged(boolean settingsChanged) {
		this.settingsChanged = settingsChanged;
	}

	public void setReverseTranslations(boolean reverseTranslations) {
		this.reverseTranslations = reverseTranslations;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public File getDefFolder() {
		return defFolder;
	}

	public File getSourceFile() {
		return sourceFile;
	}
	public boolean isHasIteratorNext() {
		return hasIteratorNext;
	}

	public ArrayList<String> getInputSet1() {
		return inputSet1;
	}

	public String getOldOutputPath() {
		return oldOutputPath;
	}
}
