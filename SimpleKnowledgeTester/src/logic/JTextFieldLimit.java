package logic;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * 
 * @author Petr Honke-Houfek
 *
 */
public class JTextFieldLimit extends PlainDocument{
	
	private int limit;
	
	public JTextFieldLimit(int limit){
		super();
		this.limit = limit;
	}
	
	@Override
	public void insertString(int offset, String s, AttributeSet atSet) throws BadLocationException {
		if(s == null){
			return;
		}
		
		if((getLength() + s.length()) <= limit){
			super.insertString(offset, s, atSet);
		}
	}
}
