package logic;

import start.Start;

/**
 * Calculates success of recent translations
 * 
 * @author Petr Honke-Houfek
 *
 */
public class SuccessCalculator {
	
	private TextFileIO textFileIO;
	private int pairsTotal;
	private int pairsCorrect;
	private double pairsPercentCorrect;
	
	public SuccessCalculator(Start start){
		textFileIO = start.getTextFileIO();
		start.setSuccessCalculator(this);
	}
	
	public String printResult(){
		pairsTotal = textFileIO.getInputSet1().size();
		pairsCorrect = pairsTotal - textFileIO.getOutputSet().size();
		if(pairsCorrect != 0){
			pairsPercentCorrect = pairsCorrect / ((double)pairsTotal / 100);
			return "You answered " + pairsCorrect + "/" + pairsTotal + "(" + (int)pairsPercentCorrect + "%) correctly !";
		}else{
			return "You answered " + pairsCorrect + "/" + pairsTotal + "(0%) correctly !";
		}
	}
	
	public int getResult(){
		pairsTotal = textFileIO.getInputSet1().size();
		pairsCorrect = pairsTotal - textFileIO.getOutputSet().size();
		if(pairsCorrect != 0){
			return (int)((double)pairsCorrect / ((double)pairsTotal / 100));
		}else{
			return 0;
		}
	}

}
