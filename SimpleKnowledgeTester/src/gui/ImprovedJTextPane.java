package gui;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 * 
 * @author Petr Honke-Houfek
 *
 */
public class ImprovedJTextPane extends JTextPane{
	
	private boolean addToEnd = true;
	
	public void addString(String s, Color c, boolean b){
		addString(s, c, b, 12);
	}
	
	public void addString(String s, Color c, boolean b, int size){
		SimpleAttributeSet set = new SimpleAttributeSet();
		StyleConstants.setForeground(set, c);
		StyleConstants.setBold(set, b);
		StyleConstants.setFontSize(set, size);
		
		if(addToEnd){
			setCaretPosition(getDocument().getLength());
		}else{
			setCaretPosition(0);
		}
		setCharacterAttributes(set, false);
		replaceSelection(s);
	}
	
	public void newLine(){
		if(addToEnd){
			setCaretPosition(getDocument().getLength());
		}else{
			setCaretPosition(0);
		}
		replaceSelection("\n");
	}
	
	public void setAddToEnd(boolean addToEnd){
		this.addToEnd = addToEnd;
	}
}
