package gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Allows only directories to be visible
 * 
 * @author Petr Honke-Houfek
 *
 */
public class SaveFilter extends FileFilter{
	
	@Override
	public boolean accept(File f){
		if(f.isDirectory()){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public String getDescription(){
		return "Only directories";
	}
}
