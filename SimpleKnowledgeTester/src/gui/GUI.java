package gui;															

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import start.Start;
import logic.DateComparator;
import logic.FileCreator;
import logic.IntegerComparator;
import logic.JTextFieldLimit;
import logic.ResultHistory;
import logic.SuccessCalculator;
import logic.TextFileIO;


/**
 * Assignment 1 GUI.java Purpose: Creating graphical user interface
 * 
 * @author Petr Honke-Houfek
 * @version 1.0 4/29/2014
 */
public class GUI extends JFrame {
	
	private Start start;
	private TextFileIO textFileIO;
	private ResultHistory resultHistory;
	private JPanel framePanel;
	private JPanel mainPanel;
	private JPanel labelPanel;
	
	private JRadioButton defFolder;
	private JRadioButton inFolder;
	private JRadioButton randFolder;
	private JRadioButton regularOrder = new JRadioButton("Regular translations");
	private JRadioButton reverseOrder = new JRadioButton("Reverse translations");
	private JButton anotherExpression;
	private JButton saveSettings;
	private JButton guide;
	private JButton addWord = new JButton("Add word");
	private JButton createFile = new JButton("Create file");
	private JButton seeHere;
	private JButton chooseLocation = new JButton("Choose");
	private JButton deleteResults = new JButton("Delete results");
	private JLabel isFileLoaded = new JLabel("File not loaded !");
	private JLabel wordLabel1;
	private JLabel wordLabel2;
	private JTextField randFolderTF = new JTextField("Enter location of desired folder here");
	private JTextField answerFieldTF;
	private JTextField newFileTF1;
	private JTextField newFileTF2;
	private JTextField newFileTF3;
	private ImprovedJTextPane newFilePane;
	private ImprovedJTextPane guidePane = new ImprovedJTextPane();
	private ImprovedJTextPane aboutPane = new ImprovedJTextPane();
	private NonEditableTableModel defTableModel = new NonEditableTableModel();
	private JTable table = new JTable(defTableModel);
	private TableRowSorter<TableModel> tableSorter = new TableRowSorter<TableModel>(table.getModel());
	private JMenuItem menuItem1 = new JMenuItem("New", new ImageIcon(getClass().getResource("/resource/newIcon.png")));
	private JMenuItem menuItem2 = new JMenuItem("Open", new ImageIcon(getClass().getResource("/resource/openIcon.png")));
	private JMenuItem menuItem3 = new JMenuItem("Exit");
	private JMenuItem menuItem4 = new JMenuItem("Translations", new ImageIcon(getClass().getResource("/resource/runIcon.png")));
	private JMenuItem menuItem6 = new JMenuItem("Results history", new ImageIcon(getClass().getResource("/resource/resultIcon.png")));
	private JMenuItem menuItem7 = new JMenuItem("Settings", new ImageIcon(getClass().getResource("/resource/settingsIcon.png")));
	private JMenuItem menuItem8 = new JMenuItem("Guide");
	private JMenuItem menuItem9 = new JMenuItem("About SKT");
	private ButtonGroup translationType = new ButtonGroup();
	private Color backColor1 = new Color(219,203,175);
	private Color backColor2 = new Color(146,128,100);
	private Color textColor = new Color(52,28,9);
	private Color textFieldColor = new Color(236,231,223);
	private JFileChooser loadFileChooser = new JFileChooser();
	private JFileChooser saveFileChooser = new JFileChooser();
	private SuccessCalculator successCalculator;
	private Font labelFont;
	private Font infoFont;
	private boolean translationInserted;
	private boolean testEnded;

	/**
	 * Constructor
	 */
	public GUI(Start start) {
		this.start = start;
		start.setGui(this);
		resultHistory = new ResultHistory(start);
		textFileIO = new TextFileIO(start);
		init();
	}

	/**
	 * Initialise GUI
	 */
	public void init() {
		translationInserted = false;
		framePanel = new JPanel(new BorderLayout());
		mainPanel = new JPanel(new GridBagLayout());
		newFilePane = new ImprovedJTextPane();
		JMenuBar jmb = new JMenuBar();
		JMenu menu1 = new JMenu("File");
		JMenu menu2 = new JMenu("Run");
		JMenu menu3 = new JMenu("View");
		JMenu menu4 = new JMenu("Help");
		seeHere = new JButton("see here");
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		
		isFileLoaded.setHorizontalAlignment(SwingConstants.CENTER);
		loadFileChooser.setFileFilter(new OpenFilter());
		saveFileChooser.setFileFilter(new SaveFilter());
		loadFileChooser.setAcceptAllFileFilterUsed(false);
		saveFileChooser.setAcceptAllFileFilterUsed(false);
		saveFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		isFileLoaded.setForeground(Color.RED);
		seeHere.setPreferredSize(new Dimension(30, 12));
		seeHere.setAlignmentY(BOTTOM_ALIGNMENT);
		seeHere.setBorder(new EmptyBorder(0, 0, 0, 0));
		seeHere.setBackground(textFieldColor);
		seeHere.setForeground(Color.BLUE);
		seeHere.setFocusPainted(false);
		chooseLocation.setBackground(textColor);
		chooseLocation.setForeground(backColor1);
		deleteResults.setBackground(textColor);
		deleteResults.setForeground(backColor1);
		deleteResults.setFocusPainted(false);
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		table.setFocusable(false);
		table.setRowSelectionAllowed(false);
		table.setRowSorter(tableSorter);
		table.getTableHeader().setReorderingAllowed(false);
		menuItem1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		menuItem2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		
		if(textFileIO.isUsingRandFolder() == true){
			randFolderTF.setText(textFileIO.getOldOutputPath());
		}
		
		guidePane.addString("Hi, thanks for using simple knowledge tester!\n\n", textColor, true, 15);
		guidePane.addString("       Translations\n", textColor, true);
		guidePane.addString( "     - First, you will need to load or create a text file with foreign words\n", textColor, false);
		guidePane.addString( "     - For creating new file choose \"File / new\", this file will be  automatically loaded to SKT, using SKT file\n"
				+ "       creator is recommended, otherwise your text file needs to have the following format ", textColor, false);
		guidePane.setCaretPosition(guidePane.getDocument().getLength());
		guidePane.insertComponent(seeHere);
		guidePane.addString( "\n     - For loading old file click \"File / Open\"\n", textColor, false);
		guidePane.addString( "     - For testing your knowledge of foreign words, click \"Run / Translations\"\n\n\n\n", textColor, false);
		guidePane.addString("Files with wrong answers will be created in desired location (see in \"View / Settings\").", textColor, false);
		aboutPane.addString("Simple Knowledge tester\n\n", textColor, true, 15);
		aboutPane.addString("Author   : Petr Honke-Houfek\nVersion  1.0\nContact : simpleknowledgetester@gmail.com", textColor, false);
		defTableModel.addColumn("Name");
		defTableModel.addColumn("Success");
		defTableModel.addColumn("Date");
		tableSorter.setComparator(1, new IntegerComparator());
		tableSorter.setComparator(2, new DateComparator());
		table.getColumnModel().getColumn(0).setCellRenderer(renderer);
		table.getColumnModel().getColumn(1).setCellRenderer(renderer);
		table.getColumnModel().getColumn(2).setCellRenderer(renderer);
		
		JTableHeader tableHeader = table.getTableHeader();
		tableHeader.setForeground(textColor);
		tableHeader.setFont(new Font("", Font.BOLD, 12));
		table.getRowSorter().toggleSortOrder(2);
		table.getRowSorter().toggleSortOrder(2);
		
		seeHere.addActionListener(new ButtonListener());
		menuItem1.addActionListener(new MenuListener());
		menuItem2.addActionListener(new MenuListener());
		menuItem3.addActionListener(new MenuListener());
		menuItem4.addActionListener(new MenuListener());
		menuItem6.addActionListener(new MenuListener());
		menuItem7.addActionListener(new MenuListener());
		menuItem8.addActionListener(new MenuListener());
		menuItem9.addActionListener(new MenuListener());
		regularOrder.addActionListener(new RadioListener());
		reverseOrder.addActionListener(new RadioListener());
		chooseLocation.addActionListener(new ButtonListener());
		deleteResults.addActionListener(new ButtonListener());
		translationType.add(regularOrder);
		translationType.add(reverseOrder);
		regularOrder.setEnabled(false);
		reverseOrder.setEnabled(false);
		jmb.add(menu1);
		jmb.add(menu2);
		jmb.add(menu3);
		jmb.add(menu4);
		menu1.add(menuItem1);
		menu1.add(menuItem2);
		menu1.addSeparator();
		menu1.add(menuItem3);
		menu2.add(menuItem4);
		menu3.add(menuItem6);
		menu3.add(menuItem7);
		menu4.add(menuItem8);
		menu4.add(menuItem9);
		framePanel.add(jmb, BorderLayout.NORTH);
		framePanel.add(mainPanel);
		add(framePanel);

		setTitle("Simple Knowledge Tester");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				textFileIO.exportExpressions(textFileIO.getOutputSet(), textFileIO.getMap());
				textFileIO.exportSettings();
				if(testEnded){
					resultHistory.addRecord();
				}
				System.exit(0);
			}
		});
		setIconImage((new ImageIcon(getClass().getResource("/resource/SKTlogo.png"))).getImage());
		setLocation(300, 150);
		setResizable(false);
		setSize(410, 280);
		setVisible(true);
		showInitialWindow();
	}

	/**
	 * Creates initial GUI
	 */
	public void showInitialWindow() {
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		translationInserted = false;
		testEnded = false;
		
		JPanel panel1 = new JPanel(new BorderLayout());
		JPanel panel2 = new JPanel(new BorderLayout());
		JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextArea localHeader = new JTextArea("Welcome to Simple Knowledge Tester");
		JTextArea localInfo = new JTextArea("\nYou can use this application for testing knowledge about :\n\n -  Translating foreign words\n -  Meaning of expressions\n\n");
		JTextArea localInstructions = new JTextArea("For using some of the functions, you need to insert text file path,\nread the instructions!\n");
		guide = new JButton("Guide");

		localHeader.setFont(new Font("", Font.BOLD, 15));
		localHeader.setFocusable(false);
		localInfo.setFocusable(false);
		localInstructions.setFocusable(false);
		localHeader.setOpaque(false);
		localInfo.setOpaque(false);
		panel1.setOpaque(false);
		panel2.setOpaque(false);
		panel3.setOpaque(false);
		localInstructions.setOpaque(false);
		localInstructions.setForeground(Color.RED);
		guide.setPreferredSize(new Dimension(66, 23));
		guide.setBackground(textColor);
		guide.setForeground(backColor1);
		guide.setFocusPainted(false);

		panel1.add(panel2, BorderLayout.NORTH);
		panel1.add(panel3, BorderLayout.CENTER);
		panel2.add(localHeader, BorderLayout.NORTH);
		panel2.add(localInfo, BorderLayout.CENTER);
		panel2.add(localInstructions, BorderLayout.SOUTH);
		panel3.add(guide);
		guide.addActionListener(new ButtonListener());
		mainPanel.add(panel1);
		mainPanel.revalidate();
		
		localInstructions.setForeground(textColor);
		localHeader.setForeground(textColor);
		localInfo.setForeground(textColor);
		mainPanel.setBackground(textFieldColor);
		
	}
	
	/**
	 * Creates GUI with informations about this application
	 */
	public void showAboutWindow(){
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new BorderLayout());
		setSize(410, 280);
		
		aboutPane.setOpaque(false);
		aboutPane.setFocusable(false);
		
		mainPanel.add(aboutPane);
	}
	
	/**
	 * Creates GUI with guide for this application
	 */
	public void showGuide1Window(){
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new BorderLayout());
		setSize(602, 340);
		
		JPanel panel1 = new JPanel(new BorderLayout());
		
		guidePane.setOpaque(false);
		guidePane.setFocusable(false);
		panel1.setOpaque(false);
		
		mainPanel.add(panel1);
		panel1.add(guidePane, BorderLayout.CENTER);
	}
	
	/**
	 * Shows picture with instructions for creating a file with foreign words
	 */
	public void showGuide2Window(){
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		GuidePanel panel1 = new GuidePanel();
		panel1.setBorder(new LineBorder(backColor2));
		
		mainPanel.add(panel1);
	}
	
	/**
	 * Creates GUI for creating your own sets of foreign words
	 */
	public void showFileCreatorWindow() {
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new BorderLayout());
		if(start.getFileCreator() == null){
			start.setFileCreator(new FileCreator(start));
			addWord.addActionListener(new ButtonListener());
		}else{
			start.getFileCreator().getMap().clear();
			start.getFileCreator().getSet().clear();
		}
		
		setSize(602, 340);
		JPanel panel1 = new JPanel(new BorderLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new BorderLayout());
		JPanel panel4 = new JPanel(new GridLayout());
		JPanel panel5 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JLabel label1 = new JLabel("First language");
		JLabel label2 = new JLabel("Second language");
		JLabel label3 = new JLabel("File name :");
		TitledBorder titledBorder1 = new TitledBorder(null, "Inserting expressions", 0, 2, new Font("", Font.PLAIN, 12), backColor2);
		TitledBorder titledBorder2 = new TitledBorder(null, "Creating file", 0, 2, new Font("", Font.PLAIN, 12), backColor2);
		GridBagConstraints gbc = new GridBagConstraints();
		newFileTF1 = new JTextField();
		newFileTF2 = new JTextField();
		newFileTF3 = new JTextField();

		gbc.insets = new Insets(0, 3, 3, 3);
		createFile.addActionListener(new ButtonListener());

		newFileTF1.setPreferredSize(new Dimension(130, 21));
		newFileTF2.setPreferredSize(new Dimension(130, 21));
		newFileTF3.setPreferredSize(new Dimension(130, 21));
		newFilePane.setPreferredSize(new Dimension(400, 150));
		newFilePane.setText("");
		newFilePane.setFocusable(false);
		newFilePane.setAddToEnd(false);
		newFilePane.setBorder(new LineBorder(backColor2, 1));
		newFileTF1.setDocument(new JTextFieldLimit(18));
		newFileTF2.setDocument(new JTextFieldLimit(18));
		newFileTF3.setDocument(new JTextFieldLimit(18));
		newFileTF3.setText("myExpressions");
		
		createFile.setBackground(textColor);
		addWord.setBackground(textColor);
		addWord.setFocusPainted(false);
		createFile.setFocusPainted(false);
		createFile.setForeground(backColor1);
		addWord.setForeground(backColor1);
		newFileTF1.setBackground(Color.WHITE);
		newFileTF2.setBackground(Color.WHITE);
		newFileTF3.setBackground(Color.WHITE);
		panel1.setBackground(textFieldColor);
		panel2.setBackground(textFieldColor);
		panel3.setBackground(textFieldColor);
		panel4.setBackground(textFieldColor);
		panel5.setBackground(textFieldColor);
		label1.setForeground(textColor);
		label2.setForeground(textColor);
		label3.setForeground(textColor);
		titledBorder1.setBorder(new LineBorder(backColor2, 1));
		titledBorder2.setBorder(new LineBorder(backColor2, 1));
		panel2.setBorder(titledBorder1);
		panel3.setBorder(new EmptyBorder(3, 0, 0, 0));
		panel4.setBorder(new EmptyBorder(0, 2, 0, 2));
		panel5.setBorder(titledBorder2);

		mainPanel.add(panel1);
		panel1.add(panel2, BorderLayout.NORTH);
		panel1.add(panel3, BorderLayout.CENTER);
		panel2.add(label1, gbc);
		panel2.add(newFileTF1, gbc);
		panel2.add(label2, gbc);
		panel2.add(newFileTF2, gbc);
		panel2.add(addWord, gbc);
		panel3.add(panel4, BorderLayout.NORTH);
		panel3.add(panel5, BorderLayout.SOUTH);
		panel4.add(newFilePane);
		panel5.add(label3, gbc);
		panel5.add(newFileTF3, gbc);
		panel5.add(createFile, gbc);
		
		mainPanel.revalidate();

	}
	
	/**
	 * Shows history of translations
	 */
	public void showResultHistoryWindow(){
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new BorderLayout());
		setSize(602, 340);
		
		JPanel panel1 = new JPanel(new BorderLayout());
		JScrollPane scroll = new JScrollPane(table);
		
		defTableModel.setRowCount(0);
		scroll.setBorder(new LineBorder(backColor2, 1));
		
		for (Object[] o : start.getResultHistory().getData()) {
			defTableModel.addRow(o);
		}
		
		mainPanel.add(panel1);
		panel1.add(scroll);
	}

	/**
	 * Creates GUI for settings
	 */
	public void showSettingsWindow() {
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new GridBagLayout());
		setSize(410, 280);

		JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel panel2 = new JPanel(new BorderLayout());
		JPanel panel3 = new JPanel(new BorderLayout());
		JPanel panel4 = new JPanel(new BorderLayout());
		JPanel panel5 = new JPanel(new GridLayout(3, 1));
		JPanel panel6 = new JPanel(new BorderLayout());
		ButtonGroup group = new ButtonGroup();
		TitledBorder titledBorder1 = new TitledBorder(null, "Inserting expressions", 0, 2, new Font("", Font.PLAIN, 12), backColor2);
		JTextArea info = new JTextArea("Select a directory for new files\n");
		defFolder = new JRadioButton("By default (C:\\Users\\username\\Simple Knowledge Tester)");
		inFolder = new JRadioButton("Same folder as inserted text file");
		randFolder = new JRadioButton("Random folder");
		saveSettings = new JButton("Save & Return");

		defFolder.addActionListener(new RadioListener());
		inFolder.addActionListener(new RadioListener());
		randFolder.addActionListener(new RadioListener());
		saveSettings.addActionListener(new ButtonListener());
		
		mainPanel.add(panel1);
		panel1.add(panel2);
		panel2.add(panel3, BorderLayout.NORTH);
		panel2.add(panel6, BorderLayout.SOUTH);
		panel3.add(info, BorderLayout.NORTH);
		panel3.add(panel5, BorderLayout.CENTER);
		panel3.add(panel4, BorderLayout.SOUTH);
		panel4.add(randFolderTF, BorderLayout.WEST);
		panel4.add(chooseLocation, BorderLayout.EAST);
		panel5.add(defFolder);
		panel5.add(inFolder);
		panel5.add(randFolder);
		panel6.add(deleteResults, BorderLayout.WEST);
		panel6.add(saveSettings, BorderLayout.EAST);
		group.add(defFolder);
		group.add(inFolder);
		group.add(randFolder);

		if (textFileIO.isUsingDefFolder() == true) {
			defFolder.setSelected(true);
			chooseLocation.setForeground(Color.GRAY);
			chooseLocation.setEnabled(false);
			chooseLocation.setBackground(textFieldColor);
		}
		if (textFileIO.isUsingInFolder() == true) {
			inFolder.setSelected(true);
			chooseLocation.setForeground(Color.GRAY);
			chooseLocation.setEnabled(false);
			chooseLocation.setBackground(textFieldColor);
		}
		if (textFileIO.isUsingRandFolder() == true) {
			randFolder.setSelected(true);
			randFolderTF.setBackground(Color.WHITE);
			randFolderTF.setFocusable(true);
			randFolderTF.requestFocus();
			randFolderTF.setOpaque(true);
			chooseLocation.setBackground(textColor);
			chooseLocation.setForeground(backColor1);
			chooseLocation.setEnabled(true);
		}
		
		panel1.setBorder(titledBorder1);
		titledBorder1.setBorder(new LineBorder(backColor2, 1));
		saveSettings.setPreferredSize(new Dimension(115, 25));
		panel2.setPreferredSize(new Dimension(360, 173));
		panel1.setOpaque(false);
		panel2.setOpaque(false);
		panel2.setOpaque(false);
		panel3.setOpaque(false);
		panel4.setOpaque(false);
		panel5.setOpaque(false);
		panel6.setOpaque(false);
		info.setOpaque(false);
		defFolder.setOpaque(false);
		inFolder.setOpaque(false);
		randFolder.setOpaque(false);
		randFolderTF.setOpaque(false);
		info.setFocusable(false);
		randFolderTF.setFocusable(false);
		
		saveSettings.setBackground(textColor);
		randFolderTF.setBackground(textFieldColor);
		saveSettings.setForeground(backColor1);
		randFolderTF.setForeground(Color.GRAY);
		info.setForeground(textColor);
		defFolder.setForeground(textColor);
		inFolder.setForeground(textColor);
		randFolder.setForeground(textColor);
		defFolder.setFont(new Font("", Font.PLAIN, getFont().getSize()));
		inFolder.setFont(new Font("", Font.PLAIN, getFont().getSize()));
		randFolder.setFont(new Font("", Font.PLAIN, getFont().getSize()));
		randFolderTF.setPreferredSize(new Dimension(278, 23));
		chooseLocation.setPreferredSize(new Dimension(78, 23));
		chooseLocation.setFocusPainted(false);
	}

	/**
	 * Creates GUI for choosing between regular or reverse translations
	 */
	public void showTranslationOptions() {
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new GridBagLayout());
		setSize(410, 280);
		
		JPanel panel1 = new JPanel(new BorderLayout());
		JPanel panel2 = new JPanel(new GridLayout(2, 1));
		
		translationType.clearSelection();
		regularOrder.setSelected(false);
		reverseOrder.setSelected(false);
		regularOrder.setOpaque(false);
		reverseOrder.setOpaque(false);
		panel1.setOpaque(false);
		panel2.setOpaque(false);
		
		mainPanel.add(panel1);
		panel1.add(panel2, BorderLayout.NORTH);
		panel1.add(isFileLoaded, BorderLayout.CENTER);
		panel2.add(regularOrder);
		panel2.add(reverseOrder);
	}
	
	/**
	 * Open a file chooser for loading a text file into SKT
	 */
	public void openFileFileChooser(){
		loadFileChooser.setCurrentDirectory(textFileIO.getDefFolder());
		int status = loadFileChooser.showOpenDialog(null);
		if(status == 0){
			textFileIO.checksFilePath(loadFileChooser.getSelectedFile().getAbsolutePath());
		}
	}
	
	/**
	 * Open a file chooser for choosing a folder for new files with wrong answers
	 */
	public void setDirectoryFileChooser(){
		saveFileChooser.setCurrentDirectory(textFileIO.getDefFolder());
		int status = saveFileChooser.showOpenDialog(null);
		if(status == 0){
			randFolderTF.setText(saveFileChooser.getSelectedFile().getAbsolutePath());
		}
	}

	/**
	 * Change GUI for translating
	 */
	public void showTranslations() {
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBackground(textFieldColor);
		setSize(410, 280);

		labelPanel = new JPanel(new GridLayout(2, 1));
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel(new BorderLayout());
		TitledBorder labelBorder1 = new TitledBorder(null, "Original", 0, 2, new Font("", Font.PLAIN, 12), textColor);
		TitledBorder labelBorder2 = new TitledBorder(null, "Translation", 0, 2, new Font("", Font.PLAIN, 12), textColor);
		wordLabel1 = new JLabel();
		wordLabel2 = new JLabel("?");
		labelFont = new Font("", Font.BOLD, 30);
		infoFont = new Font("", Font.BOLD, 15);
		answerFieldTF = new JTextField();
		anotherExpression = new JButton("Confirm");
		successCalculator = new SuccessCalculator(start);

		labelBorder1.setBorder(new LineBorder(textColor, 1));
		labelBorder2.setBorder(new LineBorder(textColor, 1));
		wordLabel1.setBorder(labelBorder1);
		wordLabel2.setBorder(labelBorder2);
		wordLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		wordLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		anotherExpression.setBackground(textColor);
		anotherExpression.setForeground(backColor1);
		anotherExpression.setFocusPainted(false);
		wordLabel1.setForeground(textColor);
		wordLabel2.setForeground(textColor);
		wordLabel1.setFont(labelFont);
		wordLabel2.setFont(labelFont);
		answerFieldTF.setPreferredSize(new Dimension(0, 27));
		answerFieldTF.setHorizontalAlignment(SwingConstants.CENTER);
		answerFieldTF.setDocument(new JTextFieldLimit(18));
		labelPanel.setOpaque(false);
		panel1.setOpaque(false);
		panel2.setOpaque(false);
		textFileIO.nextExpression();
		
		anotherExpression.addActionListener(new ButtonListener());
		answerFieldTF.addActionListener(new ButtonListener());
		labelPanel.add(wordLabel1);
		labelPanel.add(wordLabel2);
		mainPanel.add(labelPanel, BorderLayout.CENTER);
		mainPanel.add(panel1, BorderLayout.SOUTH);
		panel1.add(panel2, BorderLayout.CENTER);
		panel2.setPreferredSize(new Dimension(getWidth() - 30, 54));
		panel2.add(answerFieldTF, BorderLayout.NORTH);
		panel2.add(anotherExpression, BorderLayout.CENTER);

		answerFieldTF.requestFocus();
	}


	public JLabel isFileLoaded() {
		return isFileLoaded;
	}
	
	public JLabel getWordLabel1() {
		return wordLabel1;
	}

	public JLabel getWordLabel2() {
		return wordLabel2;
	}

	public JPanel getLabelPanel() {
		return labelPanel;
	}
	
	public JTextField getNewFileTF1() {
		return newFileTF1;
	}

	public JTextField getNewFileTF2() {
		return newFileTF2;
	}

	public JTextField getNewFileTF3() {
		return newFileTF3;
	}
	
	public ImprovedJTextPane getMyPane() {
		return newFilePane;
	}

	public JRadioButton getRegularOrder() {
		return regularOrder;
	}

	public JRadioButton getReverseOrder() {
		return reverseOrder;
	}

	public void setMyPane(ImprovedJTextPane newFileTA1) {
		this.newFilePane = newFileTA1;
	}
	
	public DefaultTableModel getDefTableModel() {
		return defTableModel;
	}


	/**
	 * JPanel with "how to" image
	 * 
	 * @author Petr Honke-Houfek
	 *
	 */
	private class GuidePanel extends JPanel{
		ImageIcon imageIcon = new ImageIcon(getClass().getResource("/resource/newFileHowTo.png"));
		Image image = imageIcon.getImage();
		
		@Override
		protected void paintComponent(Graphics g){
			g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		}
	}
	
	/**
	 * Inner class Purpose: ActionListener for RadioButtons
	 * 
	 * @author Petr Honke-Houfek
	 * 
	 */
	private class RadioListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (e.getSource().equals(defFolder)) {
				folderTFHelper(0);
			}
			if (e.getSource().equals(inFolder)) {
				folderTFHelper(0);
			}
			if (e.getSource().equals(randFolder)) {
				folderTFHelper(1);
			}
			if (e.getSource().equals(regularOrder)) {
				textFileIO.setReverseTranslations(false);
				textFileIO.restartTFIO();
				showTranslations();
			}
			if (e.getSource().equals(reverseOrder)) {
				textFileIO.setReverseTranslations(true);
				textFileIO.restartTFIO();
				showTranslations();
			}
		}

		/**
		 * Helper method for text field (random folder path)
		 * 
		 * @param i
		 */
		public void folderTFHelper(int i) {
			if (i == 0) {
				randFolderTF.setFocusable(false);
				chooseLocation.setFocusable(false);
				chooseLocation.setForeground(Color.GRAY);
				chooseLocation.setEnabled(false);
				chooseLocation.setBackground(textFieldColor);
				randFolderTF.setOpaque(false);
				if (randFolderTF.getText().trim().isEmpty() || randFolderTF.getText().equals("ERROR > You dont have permission to write in this directory!")) {
					randFolderTF.setForeground(Color.GRAY);
					randFolderTF.setText("Enter location of desired folder here");
				}
			} else {
				randFolderTF.setFocusable(true);
				chooseLocation.setFocusable(true);
				randFolderTF.requestFocus();
				randFolderTF.setOpaque(true);
				randFolderTF.setBackground(Color.WHITE);
				chooseLocation.setBackground(textColor);
				chooseLocation.setForeground(backColor1);
				chooseLocation.setEnabled(true);
				if (randFolderTF.getText().trim().equals("Enter location of desired folder here") || randFolderTF.getText().trim().equals("ERROR > You dont have permission to write in this directory!")) {
					randFolderTF.setText(null);
				}
			}
			randFolderTF.repaint();
		}
	}
	
	
	
	/**
	 * Listener for JButtons
	 * 
	 * @author Petr Honke-Houfek
	 *
	 */
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(guide)) {
				showGuide1Window();
			}
			
			if (e.getSource().equals(anotherExpression) || e.getSource().equals(answerFieldTF)) {
				if (testEnded == false) {
					if (translationInserted == false) {
						if (answerFieldTF.getText().equalsIgnoreCase(textFileIO.getMap().get(wordLabel1.getText()))) {
							wordLabel2.setText(textFileIO.getMap().get(wordLabel1.getText()));
							wordLabel2.setForeground(new Color(27,180,31));
						} else {
							wordLabel2.setText(textFileIO.getMap().get(wordLabel1.getText()));
							wordLabel2.setForeground(Color.RED);
							if (textFileIO.isOutputCreated() == false) {
								textFileIO.exportFile("wrongAnswers");
								textFileIO.setOutputCreated(true);
							}
							textFileIO.getOutputSet().add(wordLabel1.getText());
						}
						anotherExpression.setText("Next");
						translationInserted = true;
					} else {
						textFileIO.nextExpression();
						wordLabel2.setText("?");
						wordLabel2.setForeground(Color.GRAY);
						anotherExpression.setText("Confirm");
						translationInserted = false;
						answerFieldTF.setText("");
						answerFieldTF.requestFocus();
						
						if (!textFileIO.isHasIteratorNext()) {
							wordLabel1.setText("Test completed!");
							wordLabel2.setFont(infoFont);
							wordLabel2.setText(successCalculator.printResult());
							anotherExpression.setText("Restart");
							testEnded = true;
						}
					}
				} else {
					if (textFileIO.isOutputCreated() == true) {
						textFileIO.exportExpressions(textFileIO.getOutputSet(), textFileIO.getMap());
					}
					resultHistory.addRecord();
					start.restartApplication();
				}
			}
			if (e.getSource().equals(saveSettings)) {
				if (defFolder.isSelected()) {
					textFileIO.setUsingDefFolder();
					showInitialWindow();
				}
				if (inFolder.isSelected()) {
					textFileIO.setUsingInFolder();
					showInitialWindow();
				}
				if (randFolder.isSelected()) {
					File file = new File(randFolderTF.getText());
					if (file.isDirectory()) {
						boolean validDirectory = true;
						for (String o : start.getForbiddenDirectories()) {
							if (randFolderTF.getText().equalsIgnoreCase(o)) {
								validDirectory = false;
								randFolderTF.setForeground(Color.RED);
								randFolderTF.setText("ERROR > You dont have permission to write in this directory!");
							}
						}
						if (validDirectory == true) {
							textFileIO.setUsingRandFolder();
							textFileIO.setNewOutputPath(randFolderTF.getText());
							textFileIO.setSettingsChanged(true);
							showInitialWindow();
						}
					}
				}
			}
			if (e.getSource().equals(addWord)) {
				start.getFileCreator().addWord();
			}
			if (e.getSource().equals(createFile)) {
				start.getFileCreator().createFile();
			}
			if(e.getSource().equals(seeHere)){
				showGuide2Window();
			}
			if(e.getSource().equals(chooseLocation)){
				setDirectoryFileChooser();
			}
			if(e.getSource().equals(deleteResults)){
				int i = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete ALL results?", "Results delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if(i == 0){
					start.getResultHistory().deleteHistory();
				}
			}
		}
	}
	
	
	
	/**
	 * Listener for JMenu items
	 * 
	 * @author Petr Honke-Houfek
	 *
	 */
	private class MenuListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(menuItem1)) {
				showFileCreatorWindow();
			}
			if (e.getSource().equals(menuItem2)) {
				openFileFileChooser();
			}
			if (e.getSource().equals(menuItem3)) {
				textFileIO.exportExpressions(textFileIO.getOutputSet(), textFileIO.getMap());
				textFileIO.exportSettings();
				System.exit(0);
			}
			if (e.getSource().equals(menuItem4)) {
				testEnded = false;
				start.getTextFileIO().restartTFIO();
				showTranslationOptions();
			}
			if (e.getSource().equals(menuItem6)) {
				showResultHistoryWindow();
			}
			if (e.getSource().equals(menuItem7)) {
				showSettingsWindow();
				if(textFileIO.isUsingRandFolder() == false){
					if (randFolderTF.getText().isEmpty() || randFolderTF.getText().equals("ERROR > You dont have permission to write in this directory!")) {
						randFolderTF.setForeground(Color.GRAY);
						randFolderTF.setText("Enter location of desired folder here");
					}
				}
			}
			if (e.getSource().equals(menuItem8)) {
				showGuide1Window();
			}
			if (e.getSource().equals(menuItem9)) {
				showAboutWindow();
			}
		}
	}

}
