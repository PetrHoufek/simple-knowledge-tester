package gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Allows only text files to be visible
 * 
 * @author Petr Honke-Houfek
 *
 */
public class OpenFilter extends FileFilter{
	
	private final String allowedType = "txt";
	
	@Override
	public boolean accept(File f){
		if(f.isDirectory()){
			return true;
		}
		if(correctType(f)){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public String getDescription(){
		return "Only .txt files";
	}
	
	public boolean correctType(File f){
		if(f.getName().lastIndexOf('.') > 0 && f.getName().lastIndexOf('.') < f.getName().length() - 1){
			if(f.getName().substring(f.getName().lastIndexOf('.') + 1).equalsIgnoreCase(allowedType)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
