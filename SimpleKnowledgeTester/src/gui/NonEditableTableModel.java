package gui;

import javax.swing.table.DefaultTableModel;

/**
 * The purpose of this class is to later create a table with cells that are not editable
 * 
 * @author Petr Honke-Houfek
 *
 */
public class NonEditableTableModel extends DefaultTableModel{
	
	@Override
	public boolean isCellEditable(int row, int column){
		return false;
	}
	
}
